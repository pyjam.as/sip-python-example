

# CCC SIP Python Application Example

A thing we built at 36C3 to make SIP applications with python more easily.

Simple app that will host a YATE server, connect to the `voip.eventphone.de` SIP server, as a SIP client.
All calls will be handled by the python script found in `scripts/echo.py`.

The example application `echo.py` will record audio for 15 seconds (or until any button is pressed), and then play the recording back.

It works using [YATE](https://docs.yate.ro/)
and the Eventphone guys' (very nice) [python-yate library](https://github.com/eventphone/python-yate).

It is intended to be deployed with docker. After configuring, you should build the docker image anew (see Makefile)>


## Configuration

### SIP Authentication

Grab AuthUser and Password from <https://guru3.eventphone.de/extension/my> and put them into `conf.d/accfile.conf`.

### Routing to multiple applications

You can register multiple SIP numbers in `conf.d/accfile.conf`,
and route to them by adding another line in `conf.d/regexroute.conf`,
with regular expressions matching the number you intend to route
(see YATE docs for details).

### Writing your own application

You want to put a new python file into `scripts/`,
and then set up a route to it in `regexroute.conf`.
(You could also just edit `echo.py`, which is already routed).

Look at the [python-yate library](https://github.com/eventphone/python-yate) source code for how to interact with YATE.
It is documented with docstrings in the code.

You must mark your script as executable (`chmod +x myfile.py`)
and add a shebang (`#!/usr/bin/env python3`) to the top of the file.

Since YATE interacts with your program using stdout, you cannot `print()` in the usual fashion.
If you need logging for debugging (or any other purpose), you can log to a file.

## Running

Just run:

```sh
make run
```
