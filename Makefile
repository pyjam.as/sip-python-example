run: build
	docker run --name=sip-python-example -d sip-python-example 	
	docker logs -f sip-python-example 

build:
	docker build -t sip-python-example .

clean: 
	docker rm sip-python-example
