FROM pyjam/yate-docker:latest
EXPOSE 5060/udp

RUN pip install python-yate

COPY conf.d /yate-SVN/conf.d
COPY ./scripts/ /scripts/

CMD ./run -vv -CDon
